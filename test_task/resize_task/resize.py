import logging, traceback, datetime
from django.conf import settings
from images_wall.models import image
from django.views.generic import View
from django.contrib.auth.mixins import LoginRequiredMixin
from images_wall.forms import resizeForm
from django.shortcuts import render, redirect
from resize_task.tasks import makeResize
from celery.exceptions import OperationalError
from django.contrib import messages

log = logging.getLogger(__name__)

class resize(View, LoginRequiredMixin):
    def post(self, request, **w):
        imageId = w["imageId"]
        try:
            imageInstance = image.objects.get(id__exact = imageId)
        except image.DoesNotExist:
            messages.error(request, 'Изображение удалено')
            return redirect("wall", 1)
        form = resizeForm(request.POST)
        if form.has_changed() and form.is_valid():
            try:
                taskPromise = makeResize.apply_async(args=[request.user.id, imageId, settings.BASE_DIR, imageInstance.originImage.url, form.cleaned_data])
            except OperationalError as err:
                log.error('[{0}]\t{1}\r\t{2}\r------\r'.format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(err), traceback.format_exc()))
                messages.error(request, 'Сервис недоступен')
                return redirect("wall", 1)
            return redirect("showTask", taskPromise.id)
        return render(request, "images_wall/image.html", {'image': imageInstance.originImage, 'form': form, 'id': imageInstance.id})
