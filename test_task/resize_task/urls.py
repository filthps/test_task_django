from django.conf.urls import url
from resize_task.resize import resize
from resize_task.views import showActiveTask

urlpatterns = [
    url(r'active/(?P<taskId>[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12})/', showActiveTask, name = "showTask"),
    url(r'^(?P<imageId>[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12})/', resize.as_view(), name = "resize")
    ]
