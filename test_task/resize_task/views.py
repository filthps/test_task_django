import logging, traceback, datetime
from celery.result import AsyncResult
from resize_task.resize import makeResize
from django.shortcuts import render
from django.http import JsonResponse
from celery.exceptions import TimeoutError
from django.contrib.auth.decorators import login_required
from redis.exceptions import RedisError, ConnectionError
from django.conf import settings

log = logging.getLogger(__name__)

@login_required()
def showActiveTask(request, taskId):
    method = request.method
    if method == "GET":
        task = makeResize.AsyncResult(taskId)
        try:
            status = task.status
        except RedisError as er:
            status = "Сервис временно недоступен: сбой или отказ Redis"
            log.error('[{0}]\t{1}\r\t{2}\r------\r'.format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
        # 'PENDING'- неопределённое состояние
        # https://docs.celeryproject.org/en/latest/reference/celery.states.html#celery.states.PENDING
        if status == 'PENDING':
            try:
                task = task.get(timeout = 1) # Попытка найти на бэкенде
            except TimeoutError:
                # Задачи либо уже нет, либо никогда не было
                return render(request, "resize_task/taskStatus.html", {'task': None, 'status': None})
        return render(request, "resize_task/taskStatus.html", {'task': task, 'status': status})
    elif method == "POST":
        if request.is_ajax():
            task = makeResize.AsyncResult(taskId)
            try:
                status = task.status
            except RedisError as er:
                status = "Сервис временно недоступен: сбой или отказ Redis"
                log.error('[{0}]\t{1}\r\t{2}\r------\r'.format(datetime.datetime.now().strftime("%d-%b-%Y (%H:%M:%S.%f)"), str(er), traceback.format_exc()))
            if status == 'PENDING': # https://docs.celeryproject.org/en/latest/reference/celery.states.html#celery.states.PENDING
                try:
                    task = task.get(timeout = 1) # Попытка найти на бэкенде
                except TimeoutError:
                    task = None
                pass
            if task is not None:
                return JsonResponse({'taskStatus': status})
            return JsonResponse({'taskStatus': 'taskNotFound'})