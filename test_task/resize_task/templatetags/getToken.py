from django import template
from django.middleware import csrf

register = template.Library()

@register.filter
def token(request):
    return csrf.get_token(request)

