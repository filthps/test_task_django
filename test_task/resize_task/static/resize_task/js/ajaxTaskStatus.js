function mainApplication(url, timerStatus, token) {
    var doc = document;
    function getDocumentElementByClass(d, elemName) {
        var querySelector = d.querySelector || null;
        if (querySelector) {
            getDocumentElementByClass = function(d, elemName) {
                return d.querySelector("." + elemName);
            };

        } else {
            getDocumentElementByClass = function(d, elemName) {
                var elmsCollection = d.getElementsByClassName(elemName);
                return elmsCollection.length > 0 ? elmsCollection[0] : null;
            };
        }
        return getDocumentElementByClass(d, elemName);
    };
    function checkState(url, csrf) {
        var request = new XMLHttpRequest();
        request.open("POST", url, true);
        request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
        request.setRequestHeader('X-CSRFToken', csrf);
        request.onload = function(ev) {
            var target = this || event.target;
            var status = target.status;
            if (status == 200) {
                var d = doc;
                var statusPlace = getDocumentElementByClass(d, "status");
                var status = JSON.parse(target.response).taskStatus;
                statusPlace.innerText != status ? statusPlace.innerText = status : 0;
                if (status == "SUCCESS" || status == "FAILURE") {
                    clearInterval(timer);
                    getDocumentElementByClass(d, "progr").style.display = "none";
                }
            }
        };
        request.send(null);
    };
    function launchTimer(url, csrf) {
        var t = setInterval(function () {
            checkState(url, csrf);
        }, 4000);
        return t;
    };
    if (timerStatus) {
        var timer = launchTimer(url, token);
        getDocumentElementByClass(doc, "progr").style.display = "block";
    }
};
launchApp();