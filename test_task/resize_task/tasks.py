import os, io
from PIL import Image
from django.conf import settings
from django.db import Error
from django.db.transaction import atomic
from django.contrib.auth.models import User
from images_wall.models import image, resizeImage
from django.core.files.base import ContentFile
from resize_task.celery import app
from celery.exceptions import Reject

@app.task(bind = True, max_retries = 5, time_limit = 20)
def makeResize(self, userId, imageInstancePk, baseDirStr, originalImageLinkStr, imageParamsDict):
    try:
        imageInstance = image.objects.get(id__exact = imageInstancePk)
    except image.DoesNotExist:
        raise Reject('Изображение было удалено: не найдена запись в базе данных', requeue = True)
    try:
        pilImage = Image.open(os.path.normpath('{0}{1}'.format(baseDirStr, originalImageLinkStr)), mode = "r")
    except IOError:
        raise Reject('Ошибка при открытии изображения', requeue = True)
    pilImageResized = pilImage.resize((int(imageParamsDict["width"]), int(imageParamsDict["height"])), Image.BILINEAR)
    pilImage.close()
    imageName = originalImageLinkStr[originalImageLinkStr.rfind("/") + 1 : originalImageLinkStr.index(".")]
    imageFormat = originalImageLinkStr[originalImageLinkStr.index(".") + 1 : ]
    if imageFormat == "jpg" or imageFormat == "JPG":
        imageFormat = "JPEG"
    buffer = io.BytesIO()
    pilImageResized.save(buffer, imageFormat)
    byts = buffer.getvalue()
    file = ContentFile(byts)
    file.name = '{0}.{1}'.format(imageName, imageFormat)
    resizedImageInstance = imageInstance.resizedImage
    if resizedImageInstance is None:
        try:
            user = User.objects.get(id__exact = userId)
        except User.DoesNotExist:
            pass
        resizedImageInstance = resizeImage(toImage = imageInstance, resizedImage = file, by = user)
        imageInstance.resizedImage = resizedImageInstance
        try:
            # atomic гарантирует:
            # 1) Последовательность
            # 2) Либо успешны все транзакции, либо ни одной
            with atomic():
                resizedImageInstance.save()
                imageInstance.save()
        except Error:
            self.retry()
    else:
        resizedImageInstance.resizedImage = file
        try:
            resizedImageInstance.save()
        except Error:
            self.retry()
