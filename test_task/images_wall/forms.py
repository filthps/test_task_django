from django import forms
from images_wall.models import image

class resizeForm(forms.Form):
    width = forms.CharField(required = True, label = "X", widget = forms.TextInput(attrs = {'class': 'image-width'}))
    height = forms.CharField(required = True, label = "Y", widget = forms.TextInput(attrs = {'class': 'image-height'}))

    def clean_width(self):
        width = self.cleaned_data['width']
        try:
            intWidth = int(width)
        except ValueError:
            raise forms.ValidationError("Параметр - ширина должен быть целым числом.", code = "invalid")
        if intWidth < 1:
            raise forms.ValidationError("Ширина не может быть меньше 1.", code = "invalid")
        if intWidth > 9998:
            raise forms.ValidationError("Ширина не может быть больше или равна 9999.", code = "invalid")
        return width

    def clean_height(self):
        height = self.cleaned_data['height']
        try:
            intHeight = int(height)
        except ValueError:
            raise forms.ValidationError("Параметр - высота должен быть целым числом.", code = "invalid")
        if intHeight < 1:
            raise forms.ValidationError("Высота не может быть меньше 1.", code = "invalid")
        if intHeight > 9998:
            raise forms.ValidationError("Высота не может быть больше или равна 9999.", code = "invalid")
        return height

class addImageForm(forms.ModelForm):
    pass
