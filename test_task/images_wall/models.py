import datetime, uuid
from django.db import models
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFit
from django.contrib.auth.models import User


def createPathOriginal(instance, filename):
    time = datetime.datetime.now()
    return "images_wall/original/{0}-{1}-{2}-{3}/{4}".format(time.year, time.month, time.day, time.minute, filename)

def createPathResized(instacne, filename):
    time = datetime.datetime.now()
    return "images_wall/resized/{0}-{1}-{2}-{3}/{4}".format(time.year, time.month, time.day, time.minute, filename)

class image(models.Model):
    id = models.UUIDField(primary_key = True, default = uuid.uuid4)
    name = models.CharField(max_length = 25, blank = False, unique = True)
    originImage = models.ImageField(blank = False, upload_to = createPathOriginal)
    thumb = ImageSpecField(source = 'originImage', processors = [ResizeToFit(300, 300)], format='PNG', options={'quality': 60})
    addedBy = models.ForeignKey(User, null = True, on_delete = models.SET_NULL)
    addTime = models.DateTimeField(auto_now_add = True)
    resizedImage = models.OneToOneField("resizeImage", blank = True, null = True, on_delete = models.CASCADE)

    def __str__(self):
        return self.name

class resizeImage(models.Model):
    toImage = models.OneToOneField(image, primary_key = True, on_delete = models.CASCADE)
    resizedImage = models.ImageField(blank = False, upload_to = createPathResized)
    time = models.DateTimeField(auto_now_add = True)
    by = models.ForeignKey(User, related_name = "User.id+", null = True, on_delete = models.SET_NULL)

    def __str__(self):
        return self.toImage.name

