from django.conf.urls import url
from images_wall.views import showImagesWall, showImage

urlpatterns = [
    url(r'^(?P<page>[0-9]+)/$', showImagesWall.as_view(), name = "wall"),
    url(r'^(?P<imageId>[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12})/$', showImage.as_view(), name = "showImage"),
    ]
