from django.shortcuts import render
from images_wall.models import image
from images_wall.forms import resizeForm, addImageForm
from django.views.generic.base import TemplateView, View
from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib import messages

class showImagesWall(TemplateView, LoginRequiredMixin):
    template_name = "images_wall/wall.html"
    def get_context_data(self, **kw):
        imagesPaginator = Paginator(image.objects.all().order_by("-addTime"), 10, orphans = 3)
        try:
            pageItems = imagesPaginator.page(self.request.GET.get("page", 1))
        except EmptyPage:
            pass
        except PageNotAnInteger:
            pass
        context = super(showImagesWall, self).get_context_data(**kw)
        context.update({'paginator': imagesPaginator, 'items': pageItems, 'form': resizeForm()})
        return context

class showImage(TemplateView, LoginRequiredMixin):
    template_name = "images_wall/image.html"
    def get_context_data(self, **w):
        try:
            imageInstance = image.objects.get(id__exact = w["imageId"])
        except image.DoesNotExist:
            messages.error(request, 'Изображение удалено')
            return redirect("wall", 1)
        resizedImageInstance = imageInstance.resizedImage
        context = super(showImage, self).get_context_data(**w)
        if resizedImageInstance is not None:
            resizedImage = resizedImageInstance.resizedImage
            context.update({'image': resizedImage, 'form': resizeForm(initial = {'width': resizedImage.width, 'height': resizedImage.height}), 'id': imageInstance.id})
        else:
            originalImage = imageInstance.originImage
            context.update({'image': originalImage, 'form': resizeForm(initial = {'width': originalImage.width, 'height': originalImage.height}), 'id': imageInstance.id})
        return context
