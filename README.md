Тестовое задание Python

Задание: реализовать простейший асинхронный веб-сервис для «ресайзинга» картинок.
Описание: необходимо реализовать сервис, предоставляющий API (REST) для изменения размеров картинок по запросу. Данный сервис должен работать асинхронно, т.е. после получения входящего запроса на изменение этому запросу присваивается уникальный идентификатор, по которому потом можно получить статус операции. Поддерживаемые форматы изображений: jpg, png. Формат указания размера: {‘height’: h, ‘width’: w}, где h,w – размеры к которым нужно преобразовать изображение в px, оба параметра обязательны и могут принимать любые разумные значения (от 1 до 9999). Необходимо предусмотреть логирование операций сервиса, выдачу корректных ответов в случае ошибки (учесть и возврат корректного HTTP STATUS). Сервис должен предоставлять как минимум два пути (URL):
Путь для постановки задачи на изменение размера,
Путь для получения статуса задачи: принимает идентификатор задачи в качестве параметра URL и возвращает информацию о статусе задачи (или ошибку если некорректный идентификатор).
Необходимо написать юнит-тесты для реализованного сервиса. Остальное – на ваше усмотрение, но все принятые решения необходимо будет обосновать.
Технологии: 
Backend: любой из перечисленных Django, Flask, Pyramid, CherryPy, aiohttp
База данных: любая из перечисленных SQLite, PostgreSQL, MongoDB, Redis
Возможно использование Celery.
Результат задания направить в виде ссылки на открытый репозиторий в Github или Bitbucket. В письме со ссылкой на результат необходимо предоставить обоснование выбора тех или иных технологий и решений.




Зпуск производился на localhost, в среде Python Tools - Microsoft Visual Studio 2017

* Настройка

1) pip install -r requirements.txt

2) Для пуска на ОС Windows необходимо [скачать Redis 3.2.100](https://github.com/microsoftarchive/redis/releases), распаковать в любую папку и запустить *redis-server.exe*

3) Настроить базу данных Postgresql в соответствии с данными в **settings.py**

* Первый пуск

1) 
 - manage.py collectstatic
 - manage.py makemigrations
 - manage.py migrate
 - manage.py createsuperuser
 
2)
 - Вход в админку и добавление записей "images".
 
* Использование

После запуска сервера:

redis-server.exe (если windows)

запустить:
    flower --broker=redis://localhost:6379/0
    
в каталоге проекта:
    celery worker -A resize_task --loglevel=info -f celery.log --concurrency=4
    
    
![conf](https://bitbucket.org/filthps/test_task_django/raw/cf4e22bb787e0d9f4d891a807303368ead356390/readme-images/0.PNG)

    
открыть в броузере:

http://localhost:5555/ - flower


![flower](https://bitbucket.org/filthps/test_task_django/raw/cf4e22bb787e0d9f4d891a807303368ead356390/readme-images/1.PNG)

![flower1](https://bitbucket.org/filthps/test_task_django/raw/cf4e22bb787e0d9f4d891a807303368ead356390/readme-images/2.PNG)


http://localhost:3600/wall/1

См. urls покетов приложений.


![status](https://bitbucket.org/filthps/test_task_django/raw/cf4e22bb787e0d9f4d891a807303368ead356390/readme-images/3.PNG)



* Логирование
Логирование происходит в 3 уровня:
- Django 
- Приложение "resize_task"
- Лог Celery, лог операций celery (Задаётся либо в настройках юнита systemd, либо в виде параметра при вывове из терминала)